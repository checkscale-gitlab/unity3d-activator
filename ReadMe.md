# Unity Puppeteer activation script docker image

This is a docker image and a script that automates the process of requesting a `ulf` unity license from a `alf` xml file.

## How to use

* Copy dotenv from the example

```bash
cp .env.example .env
```

* Fill environment variables in `.env`
* Place your `UNITY_ACTIVATION_FILE` which contains unity xml license data at the right place.
* Run docker-compose command

```bash
docker-compose run activate
```

The resulting license will be placed in `UNITY_LICENSE_FILE`. Debugging information and screenshots can be found in `debug_images` folder.

## Why

Unity activation for personal licenses is complicated and cannot be easily automated. Since this is a docker image, it is intended to be placed in a CI Pipeline to automate unity activation process. Merge requests are welcome! :tada:

## Origin

It was initially wrote by [Alexander Buhler](https://github.com/alexanderbuhler) in [Draft: completely scripted activation #6](https://github.com/GabLeRoux/unity3d-ci-example/issues/6).

This is part of a greater project which can be found here:  
https://gitlab.com/gableroux/unity3d-gitlab-ci-example

## How are these images published to docker hub?

Refer to this [QA](https://stackoverflow.com/questions/45517733/how-to-publish-docker-images-to-docker-hub-from-gitlab-ci). tldr; I only updated a few environment variables in gitlab-ci settings, this is optional.

## How to publish a new version

```bash
npm version patch && git push && git push --tags
```

## Shameless plug

I made this for free as a gift to the video game community. If this tool helped you, feel free to become a patron for [Totema Studio](https://totemastudio.com) on Patreon: :beers:

[![Totema Studio Logo](./doc/totema-studio-logo-217.png)](https://patreon.com/totemastudio)

[![Become a Patron](./doc/become_a_patron_button.png)](https://www.patreon.com/bePatron?c=1073078)

## Want to chat?

Join us on our discord channel at [totema.studio/discord](https://totema.studio/discord), there's a technical channel in there mostly dedicated to this tool. :+1:

## License

[MIT](LICENSE.md) © [Gabriel Le Breton](https://gableroux.com)

