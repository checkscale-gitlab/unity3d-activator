#!/usr/bin/env node

const puppeteer = require('puppeteer');
const fs = require('fs');

const unityLicenseBaseUrl = 'https://license.unity3d.com';
const debug_folder = 'debug_images';
try {
    fs.mkdirSync(debug_folder);
    console.log('[activator] Created ' + debug_folder + ' directory.');
} catch (e) {
    console.log('[activator] Directory ' + debug_folder + ' is already present.');
}

onError = (err, page) => {
    page.screenshot({path: debug_folder + '/failure.png'});
    console.log(err);
    process.exit(1);
};

(async () => {
    const browser = await puppeteer.launch({
        args: ["--no-sandbox"]
    });
    const page = await browser.newPage();

    console.log('[activator] Open manual page & wait for login redirect');
    await page.goto(unityLicenseBaseUrl + '/manual')
        .catch((err) => {
            onError(err, page);
        });

    const mailInputSelector = '#conversations_create_session_form_email',
        passInputSelector = '#conversations_create_session_form_password';
    await page.waitForSelector(mailInputSelector)
        .catch((err) => {
            onError(err, page);
        });

    console.log('[activator] Enter credentials');
    await page.type(mailInputSelector, process.env.UNITY_USERNAME);
    await page.type(passInputSelector, process.env.UNITY_PASSWORD);
    await page.screenshot({path: debug_folder + '/01_entered_credentials.png'});

    console.log('[activator] Click submit');
    await page.click('input[name=commit]');

    console.log('[activator] Wait for license upload form');
    const licenseUploadfield = '#licenseFile';
    await page.waitForSelector(licenseUploadfield)
        .catch((err) => {
            onError(err, page);
        });
    await page.screenshot({path: debug_folder + '/02_opened_form.png'});

    console.log('[activator] Enable interception');
    await page.setRequestInterception(true);

    console.log('[activator] Upload license');
    page.once("request", interceptedRequest => {
        interceptedRequest.continue({
            method: "POST",
            postData: fs.readFileSync(process.env.UNITY_ACTIVATION_FILE, 'utf8'),
            headers: {"Content-Type": "text/xml"},
        }).catch((err) => {
            onError(err, page);
        });
    });

    await page.goto(unityLicenseBaseUrl + '/genesis/activation/create-transaction')
        .catch((err) => {
            onError(err, page);
        });
    await page.screenshot({path: debug_folder + '/03_created_transaction.png'});

    console.log('[activator] set license to be personal');
    page.once("request", interceptedRequest => {
        interceptedRequest.continue({
            method: "PUT",
            postData: JSON.stringify({transaction: {serial: {type: "personal"}}}),
            headers: {"Content-Type": "application/json"}
        });
    });

    await page.goto(unityLicenseBaseUrl + '/genesis/activation/update-transaction')
        .catch((err) => {
            onError(err, page);
        });
    await page.screenshot({path: debug_folder + '/04_updated_transaction.png'});

    console.log('[activator] get license content');
    page.once("request", interceptedRequest => {
        interceptedRequest.continue({
            method: "POST",
            postData: JSON.stringify({}),
            headers: {"Content-Type": "application/json"}
        });
    });

    page.on('response', async response => {
        console.log('[activator] write license');
        try {
            const data = await response.text();
            const dataJson = await JSON.parse(data);
            fs.writeFileSync(process.env.UNITY_LICENSE_FILE, dataJson.xml);
            console.log('[activator] license file written to ' + process.env.UNITY_LICENSE_FILE);
            await page.screenshot({path: debug_folder + '/05_received_license.png'});
        } catch (e) {
            console.log(e);
            console.log('[activator] failed to write license file.');
            throw e;
        }
    });

    await page.goto(unityLicenseBaseUrl + '/genesis/activation/download-license')
        .catch((err) => {
            onError(err, page);
        });
    await page.waitFor(1000);
    await browser.close();
})();

